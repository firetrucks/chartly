var Chartly = (function() {
	"use strict";
	var ReturnObj = {};

	var VerticalBarChart = function() {
		var control = {
			 centerLine  : 0,
			 bg  : undefined,
			 bgCtx  : undefined,
			 container  : undefined,
			 containerWidth  : 0,
			 containerHeight  : 0,
			 scale  : {},
			 keyMap  : {},
			 chartData  : [],
			 suffix : ""
		};

		//Constructors
		var Segment = function(key, name, val, color) {
			this.key = key;
			this.displayName = name;
			this.value = val;
			this.color = color;
			this.bar = new Bar();
		};

		var Bar = function() {
			this.height = 0;
			this.width = 0;
			this.x1 = 0;
			this.y1 = 0;
		};

		Segment.prototype.setCoordinates = function(newSegment) {
			var bar = this.bar,
				val = this.value,
				displayVal = (_scaledValue(val) * -1),
				offset = (displayVal < 0) ? -1 : 1,
				bWidth = control.scale.barWidth;

			bar.x1 = (bWidth * control.keyMap[this.key]) + 5;//Sets the padding on the end bars
			bar.y1 = offset || 0;
			bar.height = (newSegment) ? 0 : displayVal;
			bar.width = bWidth;
		};

		var init = function(parameters) {
			var exCanvas = !!window.G_vmlCanvasManager,
				parsedData,
				suf, origin,
				containerStyle, containerHeight, containerWidth,
				chartCanvas, bgCanvas;

			//Populate control object
			control.colorFactory = new ColorFactory();
			control.suffix = suf = parameters.suffix || "" + (new Date().getTime());
			control.container = document.getElementById(parameters.containerID) || document.body;
			containerStyle = (!!window.getComputedStyle) ? window.getComputedStyle(control.container, null) : control.container.currentStyle;
			containerWidth = control.containerWidth = parseInt(containerStyle.width);
			containerHeight = control.containerHeight = parseInt(containerStyle.height);
			control.legend = document.getElementById(parameters.legendID) || null;
			if(!exCanvas) {
				chartCanvas = control.chart = document.createElement("canvas");
			}

			//set the origin
			origin = parameters.origin || "center";
			switch (origin) {
				case "bottom" :
					control.centerLine = containerHeight;
					break;
				case "center" :
					control.centerLine = Math.ceil((containerHeight / 2));
					break;
				default :
					control.centerLine = Math.ceil((containerHeight / 2));
					break;
			}

			bgCanvas = control.bg = document.createElement("canvas");

			//Interlude: initialize canvases
			bgCanvas.style.position = "absolute";
			bgCanvas.id = "chartBackground" + suf;
			bgCanvas.width = containerWidth;
			bgCanvas.height = containerHeight;
			control.container.appendChild(bgCanvas);

			if(exCanvas) {
				bgCanvas = G_vmlCanvasManager.initElement(bgCanvas);
			} else {
				chartCanvas.style.position = "absolute";
				chartCanvas.id = "barChart" + suf;
				chartCanvas.width = containerWidth;
				chartCanvas.height = containerHeight;
				control.container.appendChild(chartCanvas);
				control.chartCtx = chartCanvas.getContext("2d");
				control.chartCtx.translate(0, control.centerLine);
			}
			control.bgCtx = bgCanvas.getContext("2d");

			parsedData = _parseDataAndMap(parameters.dataObj);
			control.keyMap = parsedData.map;
			control.chartData = parsedData.data;
			control.scale = _getScale();
			if (!!control.legend) {
				_drawLegend();
			}
			_drawBackground();
			_drawBars();
		};

		//Internal methods
		var _getScale = function() {
			var scale = {
					max : 10,
					step : 2,
					margin : 45,
					barWidth : 1
				},
				stepBases = [1, 2, 5],
				segments = control.chartData,
				drifts = [],
				max, exp, maxBase, maxExp, maxDecimal,
				stepBaseIdx, stepNum, segment, i, j;

			for (j=0; segment=segments[j]; j++) {
				drifts.push(segment["value"]);
			}
			max = Math.abs(drifts.sort(function(a,b){return Math.abs(b)-Math.abs(a)})[0]);
			max = (max === 0) ? 5 : max;
			maxExp = max.toExponential();
			maxDecimal = parseFloat(maxExp.slice(0, maxExp.indexOf('e')));
			maxBase = Math.floor(maxDecimal);
			maxDecimal = (maxDecimal % 1 === 0) ? (maxDecimal + 0.1) : Math.ceil(maxDecimal);
			exp = Math.round(Math.log(max/maxDecimal) / Math.LN10);
			scale.max = maxDecimal * Math.pow(10, exp);
			for(i=0; i<stepBases.length; i++){
				if(stepBases[i] > maxBase) {
					stepBaseIdx = i;
					break;
				} else {
					if(i === (stepBases.length - 1)) {
						stepBaseIdx = 0;
						exp += 1;
					}
				}
			}
			if(!!stepBases[stepBaseIdx - 2]) {
				stepNum = stepBases[stepBaseIdx - 2];
			} else {
				stepNum = stepBases[(stepBaseIdx - 2) + stepBases.length];
				exp -= 1;
			}
			scale.step = stepNum * Math.pow(10, exp);
			scale.margin = _measureText(scale.max) + 10;//10 gives the numbers a little padding
			scale.barWidth = Math.floor((control.containerWidth - (scale.margin + 10)) / segments.length);//10 allows for 5px padding on the sides of the bars on either end

			return scale;
		};

		var _measureText = function(txt) {
			var testSpan;

			//Apparently measureText doesn't work in IE8 with excanvas, so implementing the equivilent here.
			if(!control.testSpan) {
				testSpan = control.testSpan = document.createElement("span");
				testSpan.style.position = "absolute";
				testSpan.style.left = "-999px";
				testSpan.style.top = "-999px";
				testSpan.style.font = "10px Verdana,sans-serif";
				document.body.appendChild(testSpan);
			} else {
				testSpan = control.testSpan;
			}
			testSpan.innerHTML = "";
			testSpan.appendChild(document.createTextNode("-" + txt.toFixed(2)));
			return parseInt(testSpan.offsetWidth + 5);
		};

		var _scaledValue = function(value) {
			var scaledValue, scaleFactor;

			scaleFactor = control.centerLine / control.scale.max || 3;
			scaledValue = Math.floor(value * scaleFactor);

			return scaledValue;
		};

		var _drawBars = function() {
			var segments = control.chartData,
				chart = control.chartCtx || "",
				bWidth = control.scale.barWidth,
				currentChart, gradient, bar, segment,  i;

			for (i=0; segment=segments[i]; i++) {
				segment.setCoordinates(false);
				bar = segment["bar"];
				currentChart = (!!chart) ? chart : bar["canvasCtx"];
				gradient = currentChart.createLinearGradient(bar.x1, bar.y1, (bar.x1 + bWidth), bar.height);
				gradient.addColorStop(1, segment["color"]);
				gradient.addColorStop(0, "#EEEEEE");
				currentChart.fillStyle = gradient;
				currentChart.fillRect(bar.x1, bar.y1, bWidth, bar.height);
			}
		};

		var _drawBackground = function() {
			var dist,
				stepUp = control.centerLine,
				stepDown = control.centerLine,
				center = control.centerLine,
				labelInt = 0,
				bgCtx = control.bgCtx,
				width = control.bg.width,
				scalingData = control.scale,
				label, labelWidth, i;

			dist = (center / scalingData.max) * scalingData.step;

			//Draw graph interval lines and numbers
			bgCtx.beginPath();
			bgCtx.strokeStyle = "#CCCCCC";
			bgCtx.fillStyle = "#000000";
			bgCtx.font = "10px Verdana,sans-serif";
			labelWidth = scalingData.margin;
			for (i = scalingData.step; i < scalingData.max; i += scalingData.step) {
				stepDown += dist;
				stepUp -= dist;
				labelInt += scalingData.step;
				label = parseFloat(labelInt).toFixed(2);
				bgCtx.moveTo((width - labelWidth), stepDown);
				bgCtx.lineTo(0, stepDown);
				bgCtx.fillText(("-" + label), (width - (labelWidth - 10)), stepDown);
				bgCtx.moveTo((width - labelWidth), stepUp);
				bgCtx.lineTo(0, stepUp);
				bgCtx.fillText(("" + label), (width - (labelWidth - 10)), (stepUp + 9));
			}
			bgCtx.stroke();

			//Draw graph origin line and number
			bgCtx.fillStyle = "#000000";
			bgCtx.font = "10px Verdana,sans-serif";
			bgCtx.beginPath();
			bgCtx.strokeStyle = "#000000";
			bgCtx.moveTo((width - labelWidth), center);
			bgCtx.lineTo(0, center);
			bgCtx.stroke();
			bgCtx.fillText(("0.00"), (width - (labelWidth - 10)), center + 5);
		};

		var _drawLegend = function() {
			var segments = control.chartData,
				segment, color, legendList, legendItem, legendMarker, i;

			legendList = document.createElement("ul");
			legendList.style.listStyle = "none";
			legendList.style.paddingLeft = "5px";
			legendList.style.margin = "5px";
			legendList.id = "legendList";
			control.legend.appendChild(legendList);
			for (i=0; segment=segments[i]; i++ ) {
				color = control.chartData[i]["color"];
				legendItem = document.createElement("li");
				legendItem.id = segment.key + "_legendKey";
				legendList.appendChild(legendItem);
				legendMarker = document.createElement("span");
				legendItem.appendChild(legendMarker);
				legendMarker.style.backgroundColor = color;
				legendMarker.style.border = "1px solid black";
				legendMarker.style.height = "7px";
				legendMarker.style.width = "7px";
				legendMarker.style.display = "inline-block";
				legendMarker.style.marginRight = "10px";
				legendMarker.style.cssFloat = "left";
				legendMarker.style.marginTop = "3px";
				legendItem.innerHTML += control.chartData[i]["displayName"];
			}
		};

		var _parseDataAndMap = function(dataObj) {
			var chartData = dataObj || {},
				parsedData = {
					map : {},
					data : []
				},
				m = parsedData.map,
				d = parsedData.data,
				i=0,
				param, item, segment, bar, color, canvas, canvasCtx;

			//Creates an object for saving coordinates of each bar
			for (param in chartData) {
				if (chartData.hasOwnProperty(param)) {
					item = chartData[param];
					m[param] = i;
					color = item["color"] || control.colorFactory.getNewColor();
					segment = new Segment(param, item.displayName, item.value, color);
					bar = segment["bar"];
					if (!!window.G_vmlCanvasManager) {
						canvas = bar.canvas = _makeBarCanvas(param);
						control.container.appendChild(canvas);
						canvasCtx = G_vmlCanvasManager.initElement(canvas);
						canvasCtx = bar.canvasCtx = canvas.getContext("2d");
						canvasCtx.translate(0, control.centerLine);
					}
					d.push(segment);
					i++;
				}
			}

			return parsedData;
		};

		var _makeBarCanvas = function(seg) {
			var chartCanvas = document.createElement("canvas");

			chartCanvas.style.position = "absolute";
			chartCanvas.id = "canvas_" + seg + "_" + control.suffix;
			chartCanvas.width = control.containerWidth;
			chartCanvas.height = control.containerHeight;

			return chartCanvas;
		};

		var _redrawBar = function(key, val) {
			var segment = control.chartData[key],
				bar = segment["bar"],
				originalHeight = bar.height || 0,
				i = 0,
				chartCanvasCtx = control.chartCtx || bar["canvasCtx"],
				bWidth = control.scale.barWidth,
				offset, animStep, animNum, redrawTimer, gradient;

			bar.height = (_scaledValue(val) * -1);
			animStep = (bar.height - originalHeight) / 50;
			animNum = originalHeight;
			redrawTimer = window.setInterval(function() {
				chartCanvasCtx.clearRect(bar.x1, bar.y1, bar.width, (parseFloat(animNum) + parseFloat(bar.y1)));
				animNum += animStep;
				offset = (animNum < 0) ? -1 : 1;
				gradient = chartCanvasCtx.createLinearGradient(bar.x1, offset, (parseFloat(bar.x1) + parseFloat(bWidth)), animNum);
				gradient.addColorStop(1, segment.color);
				gradient.addColorStop(0, "#EEEEEE");
				chartCanvasCtx.fillStyle = gradient;
				bar.x1 = (bWidth * key) + 5;
				chartCanvasCtx.fillRect(bar.x1, offset, bWidth, animNum);
				bar.y1 = offset;
				bar.width = bWidth;
				i++;
				if (i >= 50) {
					window.clearInterval(redrawTimer);
				}
			}, 5);
		};

		//Public methods
		var updateChart = function(key, val, redraw) {
			var segIdx = control.keyMap[key],
				bars = control.chartData,
				bar,
				newScale,
				i;

			//Set new chart data and scale if needed
			try {
				bars[segIdx]["value"] = val;
			} catch (err) {
				redraw = true;
			}

			//rescale, if required
			newScale = _getScale();
			if ((control.scale.max < newScale.max) || (control.scale.max > (newScale.max * 1.5)) || redraw) {
				control.scale = newScale;
				control.bgCtx.clearRect(0, 0, control.bg.width, control.bg.height);
				if (!!control.chartCtx){
					control.chartCtx.clearRect(0, 0, control.containerWidth, control.containerHeight);
				}
				_drawBackground();
				for (i=0; bar=bars[i]; i++) {
					_redrawBar(i, bar["value"]);
				}
				return;
			}
			//otherwise redraw the single bar
			_redrawBar(segIdx, val);
		};

		var addNewBar = function(k, disp, val, col) {
			var key = k.replace(/^\s+|\s+$/g, ''),
				displayName = disp || key,
				value = val || 0,
				color = col || "",
				data = control.chartData,
				chartCanvas, chartCanvasCtx,
				segment, newBar,
				legendItem, legendMarker;

			if (!key.match(/^([a-zA-Z0-9]|\-|\_\:)+$/g)) {
				return alert("Please enter a valid HTML ID.");
			}
			if (!val.match(/^-?\d*(\.\d+)?$/)) {
				return alert("Please enter a valid number value");
			}
			for (segment in data) {
				if (data.hasOwnProperty(segment)) {
					if (key === segment) {
						return alert("This ID is already in use. Please choose a unique ID");
					}
				}
			}

			newBar = new Segment(displayName, displayName, value, color);

			//Add the new bar data and map entry. Map just uses chartData.length before adding the new bar, which accounts for it being 0-indexed. Meh.
			control.keyMap[key] = data.length;
			data.push(newBar);

			if (!color) {
				newBar["color"] = control.colorFactory.getNewColor();
			}

			if (!control.chartCtx) {
				chartCanvas = bar.canvas = document.createElement("canvas");
				chartCanvas.style.position = "absolute";
				chartCanvas.id = "canvas_" + key + "_" + control.suffix;
				chartCanvas.width = control.containerWidth;
				chartCanvas.height = control.containerHeight;
				control.container.appendChild(chartCanvas);
				chartCanvas = G_vmlCanvasManager.initElement(chartCanvas);
				chartCanvasCtx = bar.canvasCtx = chartCanvas.getContext("2d");
				chartCanvasCtx.translate(0, control.centerLine);
				newBar["canvas"] = chartCanvasCtx;
			} else {
				control.chartCtx.clearRect(0, (control.centerLine * -1), control.chart.width, control.chart.height);
			}

			if (!!control.legend) {
				legendItem = document.createElement("li");
				legendItem.id = key + "_legendKey";
				document.getElementById("legendList").appendChild(legendItem);
				legendMarker = document.createElement("span");
				legendItem.appendChild(legendMarker);
				legendMarker.style.backgroundColor = color;
				legendMarker.style.border = "1px solid black";
				legendMarker.style.height = "7px";
				legendMarker.style.width = "7px";
				legendMarker.style.display = "inline-block";
				legendMarker.style.marginRight = "10px";
				legendMarker.style.cssFloat = "left";
				legendMarker.style.marginTop = "3px";
				legendItem.innerHTML += disp;
			}

			control.scale = _getScale();
			updateChart(key, value, true);
		};

		var deleteBar = function(key) {
			var segIdx = control.keyMap[key],
			segments = control.chartData,
			keys = control.keyMap,
			segment, i, legendKey;

			control.chartData.slice(segIdx, 1);
			control.keyMap = {};
			for(i=0; segment=segments[i]; i++){
				keys[segment.key] = i;
			}
			if (!!control.legend) {
				legendKey = document.getElementById(key + "_legendKey");
				legendKey.remove();
			}
			control.chartCtx.clearRect(0, (control.centerLine * -1), control.chart.width, control.chart.height);

			control.scale = _getScale();
			updateChart();
		};

		var getSuffix = function() {
			return control.suffix;
		};

		ReturnObj.init = init;
		ReturnObj.updateChart = updateChart;
		ReturnObj.getSuffix = getSuffix;
		ReturnObj.addNewBar = addNewBar;
		ReturnObj.deleteBar = deleteBar;

		return ReturnObj;
	};

	//Private modules:
	var ColorFactory = function() {
		var ReturnObj = {},
            hsvInUse = [];
		var _rgbToHex = function(n) {
			var hex = n.toString(16);

			return (hex.length === 1) ? "0" + hex : hex;
		};

		var _rgbToHsv = function(rgb) {
			var r = rgb[0] / 255,
			g = rgb[1] / 255,
			b = rgb[2] / 255,
			max = Math.max(r, g, b),
			min = Math.min(r, g, b),
			d, h, s, v = max;

			d = max - min;
			s = (max === 0) ? 0 : d / max;

			if (max === min) {
                h = 0; // achromatic
			} else {
				switch(max) {
					case r:
						h = (g - b) / d + (g < b ? 6 : 0);
						break;
					case g:
						h = (b - r) / d + 2;
						break;
					case b:
						h = (r - g) / d + 4;
						break;
				}
				h /= 6;
			}
			return [h, s, v];
		};

		//returns true if the new color is similar to an existing color; false if it is safe to use
		var _isSimilarColor = function(rgb) {
			var hsv = _rgbToHsv(rgb),
				totalUsed = hsvInUse.length,
				i;

			for ( i = 0; i < totalUsed; i++) {
				if (totalUsed > 0 && _getColorDifference(hsv, hsvInUse[i]) < 0.17) {
					return true;
				}
			}
			return false;
		};

		//Tests the difference between two colors on the HSV model
		var _getColorDifference = function(v1, v2) {
            var i,
                d = 0;
			for ( i = 0; i < v1.length; i++) {
				d += (v1[i] - v2[i]) * (v1[i] - v2[i]);
			}
			return Math.sqrt(d);
		};

		//Generates a random color guaranteed to be some distance from existing colors
		var getRandomColor = function() {
			var rgb, ranges;

			var _getChannel = function() {
				var range = ranges.splice(Math.floor(Math.random() * ranges.length), 1)[0];
				return Math.floor(Math.random() * (range[1] - range[0])) + range[0];
			};

			while (!rgb || !!_isSimilarColor(rgb)) {
				rgb = [];
				ranges = [[150, 256], [0, 190], [0, 30]];
				rgb[0] = _getChannel();
				rgb[1] = _getChannel();
				rgb[2] = _getChannel();
			}
			hsvInUse.push(_rgbToHsv(rgb));
			return "#" + _rgbToHex(rgb[0]) + _rgbToHex(rgb[1]) + _rgbToHex(rgb[2]);
		};

		ReturnObj.getNewColor = getRandomColor;

		return ReturnObj;
	};

	ReturnObj.VerticalBarChart = VerticalBarChart;

	return ReturnObj;
}());